package org.mycompany.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis.client.Service;
import org.apache.axis.Handler;
import org.apache.axis.client.Call;
import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ObankController {
	@Autowired
	private CamelContext camelContext;

	@RequestMapping("/customerdata")
	public String customerdata(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = false, value = "Content-Type") String contentType,
			@RequestHeader(required = false, value = "authorization") String authorization,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@RequestHeader(required = false, value = "uuid") String uuid,
			@RequestParam(required = true, value = "customerId") String customerId) {
		
		System.out.println("customerdata......." + customerId);
		
		if(customerId != null) {
			try {
				createSOAPRequest(customerId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			return "error";
		}
		

		return "Success";
	}
	
	
	/**
	 * Method used to create the SOAP Request
	 */
	private static SOAPMessage createSOAPRequest(String customerId) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		
		MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", "/BusinessServices/Customer/CustProfInq/V1/Operation");
				
		SOAPPart soapPart = soapMessage.getSOAPPart();

        /*
        Construct SOAP Request Message:
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
		xmlns:v1="http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1" 
		xmlns:v11="http://www.obank.com/xsd/TW/Customer/CustProfInqRq/v1">
	<soap:Header>
		<v1:Header v1:mustUnderstand="1"><!--固定值-->
			<ChannelID>ESB</ChannelID><!--固定值-->
			<ServiceDomain>Customer</ServiceDomain><!--固定值-->
			<OperationName>CustProfInq</OperationName><!--固定值-->
 			<ConsumerID>TWESB</ConsumerID><!--固定值-->
			<TransactionID>a1b2c320190718201907181036501231</TransactionID><!--參考3.1.6-->
			<RqTimestamp>2019-07-18T12:04:53</RqTimestamp><!--此API收到request的時間-->
		</v1:Header>
	</soap:Header>
	<soap:Body>
		<v11:CustProfInqRq>
			<v11:ServiceHeader>
				<v11:TxnId>XX001</v11:TxnId><!--固定值-->
				<v11:TxnNo>T0982323</v11:TxnNo><!--固定值--> 
			</v11:ServiceHeader>
			<v11:Signon>
				<v11:CustId>IFUTBSF0001</v11:CustId><!--固定值-->
				<v11:AuthToken>0</v11:AuthToken><!--固定值-->
			</v11:Signon>
			<v11:ServiceBody>
				<!--You have a CHOICE of the next 2 items at this level-->
				<v11:CustPermId>W121400127</v11:CustPermId><!--customerId-->
				<V11:CIFNo>1000004844</V11:CIFNo><!--CIFNo-->
			</v11:ServiceBody>
		</v11:CustProfInqRq>
	</soap:Body>
</soap:Envelope>
         */

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("v1", "http://www.iisigroup.com/bank/xsd/TW/Common/SOAPENV/v1");
		envelope.addNamespaceDeclaration("v11", "http://www.obank.com/xsd/TW/Customer/CustProfInqRq/v1");

		// SOAP Header
		SOAPHeader soapHeader = envelope.getHeader();
		SOAPElement v1Header = soapHeader.addChildElement("Header", "v1");
		v1Header.setAttribute("v1:mustUnderstand", "1");
		SOAPElement ChannelID = v1Header.addChildElement("ChannelID");
		ChannelID.addTextNode("ESB");
		SOAPElement ServiceDomain = v1Header.addChildElement("ServiceDomain");
		ServiceDomain.addTextNode("Customer");
		SOAPElement OperationName = v1Header.addChildElement("OperationName");
		OperationName.addTextNode("CustProfInq");
		SOAPElement ConsumerID = v1Header.addChildElement("ConsumerID");
		ConsumerID.addTextNode("TWESB");
		SOAPElement TransactionID = v1Header.addChildElement("TransactionID");
		TransactionID.addTextNode("a1b2c320190718201907181036501231");  //Generate a new Transaction id
		SOAPElement RqTimestamp = v1Header.addChildElement("RqTimestamp");
		RqTimestamp.addTextNode("2019-07-18T12:04:53");  //Generate a new request time.
		
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement CustProfInqRq = soapBody.addChildElement("CustProfInqRq", "v11");
		SOAPElement ServiceHeader = CustProfInqRq.addChildElement("ServiceHeader", "v11");
		SOAPElement TxnId = ServiceHeader.addChildElement("TxnId", "v11");
		TxnId.addTextNode("XX001");
		SOAPElement TxnNo = ServiceHeader.addChildElement("TxnNo", "v11");
		TxnNo.addTextNode("T0982323");
		
		SOAPElement Signon = CustProfInqRq.addChildElement("Signon", "v11");
		SOAPElement CustId = Signon.addChildElement("CustId", "v11");
		CustId.addTextNode("IFUTBSF0001");
		SOAPElement AuthToken = Signon.addChildElement("AuthToken", "v11");
		AuthToken.addTextNode("0");

		SOAPElement ServiceBody = CustProfInqRq.addChildElement("ServiceBody", "v11");
		
		//<!--You have a CHOICE of the next 2 items at this level-->
		SOAPElement CustPermId = ServiceBody.addChildElement("CustPermId", "v11");
		CustPermId.addTextNode(customerId);  // param by CustPermId
		SOAPElement CIFNo = ServiceBody.addChildElement("CIFNo", "v11");
		CIFNo.addTextNode("1000004844");  // param by CIFNo
		
		soapMessage.saveChanges();

		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.println("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
	}
	
	@RequestMapping("/index")
	public String test(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = false, value = "Content-Type") String contentType,
			@RequestHeader(required = false, value = "authorization") String authorization,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@RequestHeader(required = false, value = "uuid") String uuid) {
		System.out.println("Test....................." + camelContext);
		
		
		
//		if (!"accept:application/json;charset=UTF-8".equals(contentType)) {
//			return null;
//		}
//		String transactionId = clientID + getDate() + uuid;
//		System.out.print("transactionId:::" + transactionId);

//		try {
//			String endpoint = "http://www.dneonline.com/calculator.asmx?wsdl";
//
//			Service service = new Service();
//			Call call = (Call) service.createCall();
//
//			call.setTargetEndpointAddress(endpoint);
//			call.setUseSOAPAction(true);
//			call.setSOAPActionURI("http://tempuri.org/Add");
//			call.setOperationName(new QName("http://tempuri.org/", "Add"));
//			call.addParameter("intA", org.apache.axis.encoding.XMLType.XSD_INT, javax.xml.rpc.ParameterMode.IN);// 介面的引數
//			call.addParameter("intB", org.apache.axis.encoding.XMLType.XSD_INT, javax.xml.rpc.ParameterMode.IN);// 介面的引數
//			
//			call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);// 設定返回型別
//			System.out.println(call);
//			String result = (String) call.invoke(new Object[] { 1 , 2 });
//			// 給方法傳遞引數,並且呼叫方法
//			System.out.println("result is " + result);
//			
////			String endpoint = "http://10.7.21.186:8080/webservice/commonService?wsdl";
////
////			Service service = new Service();
////			Call call = (Call) service.createCall();
////			
////			call.setTargetEndpointAddress(endpoint);
//////			call.setSOAPActionURI("http://10.7.21.186:8080/webservice/commonService");
////			call.setOperationName(new QName("http://service.example.com", "sayHello"));
////			call.addParameter("userName", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);// 介面的引數
////			
////			call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);// 設定返回型別
////			String result = (String) call.invoke(new Object[] {"howard"});
////			// 給方法傳遞引數,並且呼叫方法
////			System.out.println("result is " + result);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

//		try {
//			String routeID = "activemq-route";
//			camelContext.startRoute(routeID);
//			Thread.sleep(1000);
//			camelContext.stopRoute(routeID);
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		return "Success";
	}

	
	
	
	
	
	
	private String getDate() {
		// Get current date time
		LocalDateTime now = LocalDateTime.now();
		System.out.println("Before : " + now);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}

	@SuppressWarnings("resource")
	public static void executeSpringDSL(String routeId) throws Exception {
		new ClassPathXmlApplicationContext("classpath:spring/camel-context.xml").getBean("camel", CamelContext.class)
				.startRoute(routeId);
		Thread.sleep(2000);
		new ClassPathXmlApplicationContext("classpath:spring/camel-context.xml").getBean("camel", CamelContext.class)
				.stop();
	}
}
